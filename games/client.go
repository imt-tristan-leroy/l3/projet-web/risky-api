package games

import (
	"errors"
	"net"
)

func ConnectClient() error {
	conn, err := net.Dial(CurrentGame.serverType, CurrentGame.serverHost+":"+CurrentGame.serverPort)
	if err != nil {
		return err
	}
	CurrentGame.conn = conn
	return nil
}

func ClientWrite(cmd string) (int, error) {
	if CurrentGame.conn == nil {
		return 0, errors.New("no client connected")
	}
	return CurrentGame.conn.Write([]byte(cmd))
}

func ClientRead() (string, error) {
	if CurrentGame.conn == nil {
		return "", errors.New("no client connected")
	}
	buffer := make([]byte, 1024)
	mLen, err := CurrentGame.conn.Read(buffer)
	if err != nil {
		return "", err
	}
	return string(buffer[:mLen]), nil
}

func ClientClose() error {
	if CurrentGame.conn == nil {
		return errors.New("no client connected")
	}
	err := CurrentGame.conn.Close()
	if err != nil {
		return err
	}
	CurrentGame = nil
	return nil
}
