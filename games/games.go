package games

import (
	"github.com/google/uuid"
	"net"
	"os/exec"
	"time"
)

type Game struct {
	filePath   string
	ID         uuid.UUID
	command    *exec.Cmd
	serverType string
	serverHost string
	serverPort string
	conn       net.Conn
	UserID     string
	Ia         *IaClient
}

var CurrentGame *Game = nil

func New(filePath string) error {
	CurrentGame = &Game{
		filePath:   filePath,
		ID:         uuid.New(),
		serverHost: "localhost",
		serverPort: "14001",
		serverType: "tcp",
	}

	CurrentGame.command = exec.Command(CurrentGame.filePath)

	err := CurrentGame.command.Start()
	if err != nil {
		return err
	}
	time.Sleep(time.Second)
	return nil
}
