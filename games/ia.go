package games

import (
	"bytes"
	"os/exec"
)

type IaClient struct {
	Outb bytes.Buffer
	Errb bytes.Buffer
}

func RunIa(command string, script string) (*IaClient, error) {
	var outb, errb bytes.Buffer
	cmd := exec.Command(command, "-c", script)
	cmd.Stdout = &outb
	cmd.Stderr = &errb
	err := cmd.Run()

	return &IaClient{
		Outb: outb,
		Errb: errb,
	}, err
}
