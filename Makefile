build:
	go build -o bin/main

dev:
	go run main.go --dev

migrate:
	go run main.go --dev --migrate