package router

import (
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/server"
)

func Register(server *server.Server) {
	router := server.Router()

	RegisterAuth(router)
	RegisterUsers(router)
	RegisterIa(router)
	RegisterHistory(router)
	RegisterAuth(router)
	RegisterGame(router)
}
