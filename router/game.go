package router

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/handler/games"
)

func RegisterGame(router fiber.Router) {
	r := router.Group("games")

	r.Post("_start", games.Start)
}
