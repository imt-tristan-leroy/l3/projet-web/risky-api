package router

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/handler/ia"
)

func RegisterIa(router fiber.Router) {
	r := router.Group("ia")

	r.Post("", ia.Create)
	r.Get("", ia.GetAll)
	r.Get(":id", ia.GetById)
	r.Put(":id", ia.Update)
}
