package router

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/handler/auth"
)

func RegisterAuth(router fiber.Router) {
	r := router.Group("auth")

	r.Post("login", auth.Login)
	r.Post("token", auth.Token)
	r.Post("register", auth.Register)
}
