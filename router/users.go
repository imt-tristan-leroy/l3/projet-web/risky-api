package router

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/handler/users"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/middleware"
)

func RegisterUsers(router fiber.Router) {
	r := router.Group("users")
	r.Use(middleware.Authentication)

	r.Get("me", users.GetMe)
}
