package router

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/handler/history"
)

func RegisterHistory(router fiber.Router) {
	r := router.Group("history")

	r.Get("", history.GetAll)
}
