package ia

type PostIaDto struct {
	Name   string `json:"name" validate:"required"`
	Script string `json:"script" validate:"required"`
}
