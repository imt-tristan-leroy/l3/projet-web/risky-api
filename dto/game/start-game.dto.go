package game

type StartGameDto struct {
	UserID string `json:"userId" validate:""`
	Ia1ID  string `json:"ia1Id" validate:""`
	Ia2ID  string `json:"ia2Id" validate:""`
}
