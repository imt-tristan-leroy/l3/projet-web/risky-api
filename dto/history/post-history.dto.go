package history

type PostHistoryDto struct {
	UserID		   string    `json:"userId" validate:""`
	Ia1ID 		   string    `json:"ia1Id" validate:""`
	Ia2ID 		   string    `json:"ia2Id" validate:"required;"`
	ScorePlayer1   string    `json:"scorePlayer1" validate:"required;"`
	ScorePlayer2   string    `json:"scorePlayer2" validate:"required;"`
	Winner   	   string    `json:"winner" validate:"required;"`
}
