# Risky api

## Description

Golang api using [fiber](https://docs.gofiber.io/) library

### Project architecture

main.go is the entry point of the project it starts the database connection and run the migration if the parameter `--migrate` is used.  
It also starts the server and with .env file if the `--dev` parameter is passed when start the app.  

#### Server
Contain the code to start,close and handle the server fiber server

#### Database
Contain code to the different models and repositories using the [gorm](https://gorm.io/index.html) orm

#### DTO
Contain the data transfer object struct to validate body and query param type

#### Handler
Contain the handler for the different resource of the application

#### Router
Contain the code to register the different routes

## Installation

```bash
cp .en.dist .env
```

## Running migration

```bash
# Run migration in development mode
go run main.go --dev --migrate

# Run migration in production mode
go run main.go --migrate
```

## Running the app

```bash
# Start in development mode
go run main.go --dev

# Start in production mode
go run main.go
```

## Build the app

```bash
go build -o ./build
```
