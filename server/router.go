package server

import "github.com/gofiber/fiber/v2"

func (server *Server) Router() fiber.Router {
	return server.app.Group("v1")
}
