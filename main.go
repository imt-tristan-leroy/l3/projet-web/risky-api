package main

import (
	"flag"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/joho/godotenv"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database/models"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/handler"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/middleware"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/router"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/server"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"time"
)

func main() {
	// Flags
	dev := flag.Bool("dev", false, "Start in development mode")
	migrate := flag.Bool("migrate", false, "Start gorm automigrate")
	flag.Parse()

	// Load env
	if *dev {
		err := godotenv.Load()
		if err != nil {
			log.Fatal("Error loading .env file")
		}
	}

	// Database
	dsn := "host=" + os.Getenv("GORM_HOST")
	dsn = dsn + " user=" + os.Getenv("GORM_USER")
	dsn = dsn + " password=" + os.Getenv("GORM_PASSWORD")
	dsn = dsn + " dbname=" + os.Getenv("GORM_DATABASE")
	dsn = dsn + " port=" + os.Getenv("GORM_PORT")
	err := database.Init(postgres.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Silent),
	})
	if err != nil {
		log.Fatal(err)
	}

	// Run gorm migration
	if *migrate {
		err := database.DB.AutoMigrate(models.User{}, models.Ia{}, models.History{})
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Migration complete")
		return
	}

	// Fiber app
	app := server.New(fiber.Config{
		WriteTimeout: 10 * time.Second,
		ReadTimeout:  10 * time.Second,
		ErrorHandler: handler.Error,
	})

	// Register middleware
	app.Use(cors.New())
	app.Use(middleware.Jwt)

	// Register router
	router.Register(app)

	if err := app.ListenWithGraceful(os.Getenv("PORT")); err != nil {
		log.Fatal(err)
	}

	app.CleanupTask()
}
