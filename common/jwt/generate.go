package jwt

import (
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"time"
)

func GenerateTokens(userId string) (accessToken string, refreshToken string) {
	accessToken = generateAccessToken(userId)
	refreshToken = generateRefreshToken(userId)
	return accessToken, refreshToken
}

func generateAccessToken(userId string) string {
	key, err := PrivateKey()
	if err != nil {
		panic(err)
	}

	claim := Claims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    userId,
			ExpiresAt: time.Now().Add(15 * time.Minute).Unix(),
			Subject:   "access_token",
			IssuedAt:  time.Now().Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claim)
	tokenString, err := token.SignedString(key)
	if err != nil {
		panic(err)
	}

	return tokenString
}

func generateRefreshToken(userId string) string {
	key, err := PrivateKey()
	if err != nil {
		panic(err)
	}

	claim := Claims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    userId,
			ExpiresAt: time.Now().Add(7 * 24 * time.Hour).Unix(),
			Subject:   "refresh_token",
			IssuedAt:  time.Now().Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claim)
	tokenString, err := token.SignedString(key)
	if err != nil {
		panic(err)
	}

	return tokenString
}

func GenerateRefreshCookie(refreshToken string) *fiber.Cookie {
	return &fiber.Cookie{
		Name:     "refreshToken",
		Value:    refreshToken,
		Expires:  time.Now().Add(7 * 24 * time.Hour),
		HTTPOnly: true,
		Secure:   false,
	}
}
