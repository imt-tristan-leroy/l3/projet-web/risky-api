package jwt

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"os"
)

func PublicKey() (*rsa.PublicKey, error) {
	block, _ := pem.Decode([]byte(os.Getenv("JWT_PUBLIC_KEY")))
	if block == nil || block.Type != "PUBLIC KEY" {
		return nil, errors.New("failed to parse PEM block containing public key")
	}

	key, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	return key.(*rsa.PublicKey), nil
}

func PrivateKey() (*rsa.PrivateKey, error) {
	block, _ := pem.Decode([]byte(os.Getenv("JWT_PRIVATE_KEY")))
	if block == nil || block.Type != "RSA PRIVATE KEY" {
		return nil, errors.New("failed to parse PEM block containing private key")
	}

	key, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	return key, nil
}
