package jwt

import (
	"github.com/golang-jwt/jwt/v4"
)

func Parse(tokenString string) (*jwt.Token, *Claims, error) {
	key, err := PublicKey()
	if err != nil {
		panic(err)
	}

	claims := new(Claims)
	token, err := jwt.ParseWithClaims(tokenString, claims,
		func(token *jwt.Token) (interface{}, error) {
			return key, nil
		})
	return token, claims, err
}
