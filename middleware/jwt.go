package middleware

import (
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/common/jwt"
	usersRepository "gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database/repositories/users"
	"strings"
	"time"
)

func Jwt(ctx *fiber.Ctx) error {
	ctx.Locals("user", nil)
	ctx.Locals("authError", "")

	authorization := ctx.Get("Authorization")

	if authorization == "" {
		ctx.Locals("authError", "No authorization header found")
		return ctx.Next()
	}

	if !strings.HasPrefix(authorization, "Bearer ") {
		ctx.Locals("authError", "Wrong authorization header")
		return ctx.Next()
	}

	tokenString := strings.Split(authorization, " ")[1]

	token, claims, err := jwt.Parse(tokenString)
	if err != nil {
		ctx.Locals("authError", err.Error())
		return ctx.Next()
	}

	if !token.Valid {
		ctx.Locals("authError", "Token is not valid")
		return ctx.Next()
	}

	if claims.ExpiresAt < time.Now().Unix() {
		ctx.Locals("authError", "Token is expired")
		return ctx.Next()
	}

	user := usersRepository.FindOneById(claims.Issuer)

	if user.ID == uuid.Nil {
		ctx.Locals("authError", "No user authenticated")
		return ctx.Next()
	}

	ctx.Locals("user", user)

	return ctx.Next()
}
