package middleware

import (
	"github.com/gofiber/fiber/v2"
)

func Authentication(ctx *fiber.Ctx) error {
	if ctx.Locals("authError") != "" || ctx.Locals("user") == nil {
		err := ctx.Locals("authError").(string)
		return fiber.NewError(fiber.StatusUnauthorized, err)
	}

	return ctx.Next()
}
