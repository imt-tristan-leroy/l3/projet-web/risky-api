package history

import (
	"github.com/gofiber/fiber/v2"
	historyRepository "gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database/repositories/history"
)

func GetAll(ctx *fiber.Ctx) error {

	history := historyRepository.FindAll()

	return ctx.JSON(history)
}
