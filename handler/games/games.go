package games

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/common/validation"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database/models"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database/repositories/history"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database/repositories/ia"
	dto "gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/dto/game"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/games"
	"os"
	"strings"
)

func Start(ctx *fiber.Ctx) error {

	data := new(dto.StartGameDto)

	if err := ctx.BodyParser(data); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	errors := validation.Validate(*data)
	if errors != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(errors)
	}

	err := games.New(os.Getenv("RISKY_BINARY_PATH"))
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}

	var ia1, ia2 *models.Ia
	ia2 = ia.FindOneById(data.Ia2ID)

	ia2Ch := make(chan string)
	ia1Ch := make(chan string)

	go func() {
		iaClient, err := games.RunIa("python3", ia2.Script)
		if err != nil {
			//return fiber.NewError(fiber.StatusInternalServerError, err.Error())
		}
		games.CurrentGame.Ia = iaClient
		ia2Ch <- games.CurrentGame.Ia.Outb.String()
	}()

	if data.UserID != "" {
		games.CurrentGame.UserID = data.UserID
		err := games.ConnectClient()
		if err != nil {
			return fiber.NewError(fiber.StatusInternalServerError, err.Error())
		}
	} else if data.Ia1ID != "" {

		go func() {
			ia1 = ia.FindOneById(data.Ia1ID)
			secondIaClient, err := games.RunIa("python3", ia1.Script)
			if err != nil {
				//return fiber.NewError(fiber.StatusInternalServerError, err.Error())
			}
			ia1Ch <- secondIaClient.Outb.String()
		}()
	}

	ia1Str := <-ia1Ch
	ia2Str := <-ia2Ch

	ia1Actions := strings.Split(ia1Str, "\n")
	ia2Actions := strings.Split(ia2Str, "\n")

	ia1Final := ia1Actions[len(ia1Actions)-2]
	ia2Final := ia2Actions[len(ia2Actions)-2]

	ia1FinalResult := strings.Split(ia1Final, " ")
	ia2FinalResult := strings.Split(ia2Final, " ")

	var winner string

	if ia1FinalResult[len(ia1FinalResult)-1] == "1" {
		winner = "player 1"
	} else {
		winner = "player 2"
	}

	history.Create(&models.History{
		Ia1ID:  ia1.ID,
		Ia2ID:  ia2.ID,
		Winner: winner,
	})

	return ctx.JSON(fiber.Map{
		"end": fiber.Map{
			"player1": ia1FinalResult[len(ia1FinalResult)-1] == "1",
			"player2": ia2FinalResult[len(ia2FinalResult)-1] == "1",
		},
	})
}
