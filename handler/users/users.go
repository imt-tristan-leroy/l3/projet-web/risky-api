package users

import (
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database/models"
)

func GetMe(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*models.User)

	if user.ID == uuid.Nil {
		return fiber.NewError(fiber.StatusNotFound, "User not authenticated")
	}
	return ctx.JSON(user)
}
