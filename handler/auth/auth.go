package auth

import (
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/common/jwt"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/common/validation"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database/models"
	usersRepository "gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database/repositories/users"
	dto "gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/dto/auth"
	"golang.org/x/crypto/bcrypt"
	"time"
)

func Login(ctx *fiber.Ctx) error {
	data := new(dto.LoginDto)

	if err := ctx.BodyParser(data); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	errors := validation.Validate(*data)
	if errors != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(errors)
	}

	me := usersRepository.FindOneByEmail(data.Email)

	if me.ID == uuid.Nil || bcrypt.CompareHashAndPassword(
		[]byte(me.Password),
		[]byte(data.Password),
	) != nil {
		return fiber.NewError(fiber.StatusUnauthorized, "Invalid email or password")
	}

	accessToken, refreshToken := jwt.GenerateTokens(me.ID.String())

	ctx.Cookie(jwt.GenerateRefreshCookie(refreshToken))

	return ctx.JSON(fiber.Map{
		"accessToken":  accessToken,
		"refreshToken": refreshToken,
	})
}

func Token(ctx *fiber.Ctx) error {
	refreshTokenString := ctx.Cookies("refreshToken")

	if refreshTokenString == "" {
		return fiber.NewError(fiber.StatusBadRequest, "No Refresh Token")
	}
	token, claims, err := jwt.Parse(refreshTokenString)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}

	if !token.Valid {
		return fiber.NewError(fiber.StatusBadRequest, "Invalid Refresh Token")
	}

	if claims.ExpiresAt < time.Now().Unix() {
		return fiber.NewError(fiber.StatusBadRequest, "Refresh Token expired")
	}

	accessToken, refreshToken := jwt.GenerateTokens(claims.Issuer)

	ctx.Cookie(jwt.GenerateRefreshCookie(refreshToken))

	return ctx.JSON(fiber.Map{
		"accessToken":  accessToken,
		"refreshToken": refreshToken,
	})
}

func Register(ctx *fiber.Ctx) error {
	data := new(dto.RegisterDto)

	if err := ctx.BodyParser(data); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	errors := validation.Validate(*data)
	if errors != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(errors)
	}

	hashPassword, err := bcrypt.GenerateFromPassword([]byte(data.Password), 14)
	if err != nil {
		return err
	}

	me := usersRepository.Create(&models.User{
		Email:    data.Email,
		Password: string(hashPassword),
	})

	if me.ID == uuid.Nil {
		user := usersRepository.FindOneByEmail(data.Email)
		if user.ID != uuid.Nil {
			return fiber.NewError(fiber.StatusConflict, "Email is already register")
		}
		return fiber.NewError(fiber.StatusInternalServerError, "Cannot create users")
	}

	accessToken, refreshToken := jwt.GenerateTokens(me.ID.String())

	ctx.Cookie(jwt.GenerateRefreshCookie(refreshToken))

	return ctx.JSON(fiber.Map{
		"accessToken":  accessToken,
		"refreshToken": refreshToken,
	})
}
