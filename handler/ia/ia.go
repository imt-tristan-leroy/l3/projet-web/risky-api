package ia

import (
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/common/validation"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database/models"
	iaRepository "gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database/repositories/ia"
	dto "gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/dto/ia"
)

func Create(ctx *fiber.Ctx) error {
	data := new(dto.PostIaDto)

	if err := ctx.BodyParser(data); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	errors := validation.Validate(*data)
	if errors != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(errors)
	}

	ia := iaRepository.Create(&models.Ia{
		Name:   data.Name,
		Script: data.Script,
	})

	if ia.ID == uuid.Nil {
		return fiber.NewError(fiber.StatusInternalServerError, "Cannot create ia")
	}

	return ctx.JSON(ia)
}

func GetAll(ctx *fiber.Ctx) error {

	ia := iaRepository.FindAll()

	return ctx.JSON(ia)
}

func GetById(ctx *fiber.Ctx) error {
	id := ctx.Params("id")

	ia := iaRepository.FindOneById(id)
	if ia.ID == uuid.Nil {
		return fiber.NewError(fiber.StatusNotFound, "Ia "+id+" was not found")
	}

	return ctx.JSON(ia)
}

func Update(ctx *fiber.Ctx) error {
	id := ctx.Params("id")

	data := new(dto.PutIaDto)

	if err := ctx.BodyParser(data); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	errors := validation.Validate(*data)
	if errors != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(errors)
	}

	ia := iaRepository.FindOneById(id)

	if ia.ID == uuid.Nil {
		return fiber.NewError(fiber.StatusNotFound, "Cannot found ia "+id)
	}

	iaRepository.Update(ia, &models.Ia{
		Name:   data.Name,
		Script: ia.Script,
	})

	return ctx.JSON(ia)
}
