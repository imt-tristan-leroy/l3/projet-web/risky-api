package user

import (
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database/models"
)

func FindOneById(id string) (user *models.User) {
	database.DB.Where("id = ?", id).First(&user)
	return
}

func FindOneByEmail(email string) (user *models.User) {
	database.DB.Where("email = ?", email).First(&user)
	return
}

func Create(user *models.User) *models.User {
	database.DB.Create(&user)
	return user
}
