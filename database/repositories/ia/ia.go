package ia

import (
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database/models"
)

func FindOneById(id string) (ia *models.Ia) {
	database.DB.Where("id = ?", id).First(&ia)
	return
}

func Create(ia *models.Ia) *models.Ia {
	database.DB.Create(&ia)
	return ia
}

func FindAll() (ia []*models.Ia) {
	database.DB.Find(&ia)
	return
}

func Update(oldIa *models.Ia, newIa *models.Ia) {
	database.DB.Model(&oldIa).Updates(&newIa)
}