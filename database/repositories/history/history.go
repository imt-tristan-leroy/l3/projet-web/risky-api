package history

import (
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database"
	"gitlab.com/imt-tristan-leroy/l3/projet-web/risky-api/database/models"
)

func Create(history *models.History) *models.History {
	database.DB.Create(&history)
	return history
}

func FindAll() (history []*models.History) {
	database.DB.Order("date desc").Find(&history)
	return
}
