package models

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type User struct {
	ID       uuid.UUID `json:"id" gorm:"primaryKey"`
	Email    string    `json:"email" gorm:"required;unique"`
	Password string    `json:"-" gorm:"required"`
}

func (user *User) BeforeCreate(_ *gorm.DB) (err error) {
	if user.ID == uuid.Nil {
		user.ID = uuid.New()
	}
	return
}
