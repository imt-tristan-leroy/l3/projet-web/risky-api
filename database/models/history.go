package models

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"time"
)

type History struct {
	ID           uuid.UUID `json:"id" gorm:"primaryKey"`
	UserID       uuid.UUID `json:"userId" gorm:""`
	Ia1ID        uuid.UUID `json:"ia1Id" gorm:""`
	Ia2ID        uuid.UUID `json:"ia2Id" gorm:"required;"`
	ScorePlayer1 string    `json:"scorePlayer1" gorm:""`
	ScorePlayer2 string    `json:"scorePlayer2" gorm:""`
	Date         string    `json:"date" gorm:"required;"`
	Winner       string    `json:"winner" gorm:"required;"`
}

func (history *History) BeforeCreate(_ *gorm.DB) (err error) {
	if history.ID == uuid.Nil {
		history.ID = uuid.New()
	}
	if history.Date == "" {
		currentTime := time.Now()
		history.Date = currentTime.Format("02-01-2006 15:04:05")
	}
	return
}
