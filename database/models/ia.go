package models

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Ia struct {
	ID     uuid.UUID `json:"id" gorm:"primaryKey"`
	Name   string    `json:"name" gorm:"required;"`
	Script string    `json:"script" gorm:"required"`
}

func (ia *Ia) BeforeCreate(_ *gorm.DB) (err error) {
	if ia.ID == uuid.Nil {
		ia.ID = uuid.New()
	}
	return
}
